import os
from tornado.auth import OAuth2Mixin
from oauthenticator.generic import GenericOAuthenticator
from oauthenticator.oauth2 import OAuthLoginHandler

from val_auth.valeria_authenticator import ValeriaAuthenticator

c.OAuthenticator.oauth_callback_url = 'http://localhost:8000/hub/oauth_callback'

# Taken from https://stackoverflow.com/questions/56826344/error-integrating-jupyterhub-with-keycloak-auth

# Note: will be in ansible script


class KeycloakMixin(OAuth2Mixin):
    # callback_url
    _OAUTH_AUTHORIZE_URL = '{}/auth'.format(os.environ['OAUTH_URL'])
    _OAUTH_ACCESS_TOKEN_URL = '{}/token'.format(os.environ['OAUTH_URL'])

# Note: will be in ansible script


class KeycloakLoginHandler(OAuthLoginHandler, KeycloakMixin):
    pass


class ConcreteValeriaAuthenticator(ValeriaAuthenticator):
    login_service = 'Keycloak'
    login_handler = KeycloakLoginHandler
    client_id = os.environ['CLIENT_ID']
    client_secret = os.environ['CLIENT_SECRET']
    token_url = '{}/token'.format(os.environ['OAUTH_URL'])
    userdata_url = '{}/userinfo'.format(os.environ['OAUTH_URL'])
    userdata_method = 'GET'
    userdata_params = {"state": "state"}
    username_key = "preferred_username"

    val_services_base_url = os.environ['VAL_SERVICES']


# Activation
c.JupyterHub.authenticator_class = ConcreteValeriaAuthenticator
