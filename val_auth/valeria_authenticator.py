import os

from traitlets import Unicode

from oauthenticator.generic import GenericOAuthenticator
from val_auth.val_services_api import ValServicesApi


class ValeriaAuthenticator(GenericOAuthenticator):
    val_services_base_url = Unicode(
        "http://localhost:8080/",
        config=True,
        help="Endpoint val-services qui autorise ou non l'accès au hub",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.val_services_api = ValServicesApi(self.val_services_base_url)

    async def pre_spawn_start(self, user, spawner):
        auth_state = await user.get_auth_state()
        if not auth_state:
            self.log.warning("AUTH_STATE NOT ENABLED?")
            return
        spawner.environment['VAL_OAUTH_TOKEN'] = auth_state['access_token']

    async def authenticate(self, handler, data=None):
        authentication = await super().authenticate(handler, data)

        auth_state = authentication['auth_state']
        access_token = auth_state['access_token']

        login_result = self.val_services_api.get_hpc(access_token)

        # Keep logic simple for now don't mage http request codes.
        if login_result is not None:
            username = auth_state['oauth_user']['preferred_username']
            init_script = "/usr/local/bin/setup_valeria_50_accounting.sh"
            if os.path.isfile(init_script):
                os.system('{} -u {}'.format(init_script, username))
            return authentication

        return None
