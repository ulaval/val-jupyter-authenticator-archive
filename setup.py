from setuptools import setup


def main():
    setup(
        name="valeria_authenticator",
        packages=['val_auth'],
        version="0.0.8",
        install_requires=[
            'oauthenticator @ git+https://git@github.com/pylanglois/oauthenticator_contrib.git@id_token_hint_redirect'
        ],

    )


if __name__ == '__main__':
    main()
