import json
import requests


class ValServicesApi:
    def __init__(self, api_url_base):
        self.api_url_base = api_url_base

    def get_headers(self, api_token):
        return {'Content-Type': 'application/json',
                'Authorization': 'Bearer {0}'.format(api_token)}

    def get_hpc(self, api_token):

        api_url = '{0}val/v1/hpc'.format(
            self.api_url_base)

        response = requests.get(api_url,
                                headers=self.get_headers(api_token))

        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
        else:
            return None


if __name__ == "__main__":
    api_token = ''
    val_services_api = ValServicesApi('https://services.dev.valeria.science/')

    print(val_services_api.get_hpc(api_token, "test@gmail.com"))
